This is **DEPRECATED**! Please go to https://github.com/particle4dev/docker-node

### HOW TO ADD NEW VERSION

- Step1: Open .gitlab-ci.yml file

- Step2: Add version you wanted in bottom the file. E.g

```
node-8.9.3-dumb-1.2.1: *build_and_deploy_custom
```

### LINKS

- https://github.com/nodejs/docker-node

- https://gitlab.com/particle4dev/build-images
